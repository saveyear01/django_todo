from django.urls import path, include
from rest_framework import routers
from . import views

app_name = 'todo'
urlpatterns = [
    path('tasklists/', views.TasklistsView.as_view({
        'get': 'get',
        'post': 'create'
    })),
    path('tasks/<int:id>', views.TaskView.as_view({
        'get': 'get',
        'post': 'create',
    })),
    path('task/<int:id>', views.TaskDetails.as_view({
        'get': 'get',
        'put': 'put'
    })),

    # path('', views.lists.as_view(), name="todo-home"),
    # path('create-list/', views.list_form.as_view(), name="todo-create-list"),
    # path('create-task/<int:id>', views.taskCreate.as_view(), name="todo-create-task"),
    # path('tasks/<int:id>', views.tasks.as_view(), name="todo-tasks"),
    # path('tasks/<int:id>/delete', views.deletelist.as_view(), name="todo-listdelete"),
    # path('task/<int:id>', views.tasks.as_view(), name="todo-task"),
    # path('task/<int:id>/delete', views.deletetask.as_view(), name="todo-taskdelete"),
]
