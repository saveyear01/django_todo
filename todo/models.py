from django.db import models
from django.contrib.auth.models import User
from django.urls import reverse

class TaskList(models.Model):
    name = models.CharField(max_length=50)
    user = models.ForeignKey(User, on_delete=models.CASCADE) 
    
    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse("todo:todo-tasks", kwargs={'id': self.id})

class Task(models.Model):
    name = models.CharField(max_length=50)
    details = models.TextField(null=True, blank=True)
    startDate = models.DateTimeField(null=True, blank = True)
    isDone = models.BooleanField(default=False)
    TaskList = models.ForeignKey(TaskList, on_delete=models.CASCADE) 
    
    def __str__(self):
        return self.name
    
    def get_absolute_url(self):
        return reverse("todo:todo-task", kwargs={'id': self.id})


class SubTask(models.Model):
    name = models.CharField(max_length=50)
    isDone = models.BooleanField()
    task =  models.ForeignKey(Task, on_delete=models.CASCADE)

    def __str__(self):
        return self.name