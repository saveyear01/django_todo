from .models import Task, TaskList
from django.contrib.auth.models import User
from rest_framework import serializers


class TasklistSerializer(serializers.ModelSerializer):
    
    class Meta:
        model = TaskList
        fields = ['id', 'name', 'user']

class TaskSerializer(serializers.ModelSerializer):
    TaskList = TasklistSerializer(read_only=True)
    class Meta:
        model = Task
        fields = ['id', 'name','details', 'startDate', 'isDone', 'TaskList']
    
    def __init__(self, *args, **kwargs):
        self.tasklist = kwargs.pop('TaskList', None)
        return super(TaskSerializer, self).__init__(*args, **kwargs)
    
    def create(self, data):
        # import pdb; pdb.set_trace()
        return self.Meta.model.objects.create(
            TaskList=self.tasklist,
            **data,
        )
