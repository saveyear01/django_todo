from django.contrib.auth.models import User, Group
from django.http import HttpResponse, JsonResponse, Http404

from rest_framework.viewsets import ViewSet
from rest_framework.response import Response


from .serializers import TaskSerializer, TasklistSerializer
from .models import Task, TaskList

class TasklistsView(ViewSet):
    serializer_class = TasklistSerializer

    def get(self, request):
        serializer = self.serializer_class(
            self.serializer_class.Meta.model.objects.filter(user=request.user),
            many=True
        )
        
        return Response(serializer.data, status=200)
    
    def create(self, request):
        serializer = self.serializer_class(data=request.data, user=request.user)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        
        return Response(serializer.data, status=200)

class TaskView(ViewSet):
    serializer_class = TaskSerializer
    
    def get(self, request, id):
        serializer = self.serializer_class(
            self.serializer_class.Meta.model.objects.filter(TaskList=id), 
            many=True
        )
        return Response(serializer.data, status=200)

    def create(self, request, id):
        data = request.data
        serializer = self.serializer_class(data=request.data, TaskList = TasklistSerializer.Meta.model.objects.get(id=id),)
        serializer.is_valid(raise_exception=True)
        serializer.save()

        return Response(serializer.data, status=200)

class TaskDetails(ViewSet):

    serializer_class = TaskSerializer

    def getTask(self, id):
        try:
            return Task.objects.get(id=id)
        except Task.DoesNotExist:
            raise Http404

    def get(self, request, id):    
        serializer = self.serializer_class(self.getTask(id))
        return Response(serializer.data)
    
    def put(self, request, id):
        serializer = self.serializer_class(self.getTask(id), data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=400)
    
    def delete(self, request, id):
        task = self.getTask(id)
        task.delete()
        return Response(status=204)
