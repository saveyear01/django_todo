from django import forms

class createList(forms.Form):
    name = forms.CharField(max_length=50, widget=forms.TextInput(attrs={'class': 'form-control'}))

class createTask(forms.Form):
    name = forms.CharField(max_length=50, widget=forms.TextInput(attrs={'class': 'form-control'}))
    details = forms.CharField(max_length=500, widget=forms.Textarea(attrs={'class': 'form-control'}))